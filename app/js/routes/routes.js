define(['jquery', 'underscore', 'backbone', 'collectionView', 'collection'],
	function($, _, Backbone, collectionView, collection) {
		var Router = Backbone.Router.extend({
			initialize: function() {
				console.log("Routes Init.");
			},

			routes: {
				"query": "queryList",
				"reset": "resetList",
			},

			queryList: function() {
				new collectionView({
					collection: new collection,
					el: "#searchResults"
				});
			},

			resetList: function() {
				console.log("Reset List");
			}
		});
		return Router;
	});
