define(['jquery', 'underscore', 'backbone', 'collection', 'currentView', 'model', 'ageModel'], 
	function($, _, Backbone, collection, currentView, model, ageModel) {
		var formView = Backbone.View.extend({
			initialize: function() {
				this.ageMin = $("ageMin");
				this.ageMax = $("ageMax");
				this.collection = new collection;
				this.model.on('change', this.render, this);
				this.render();
			},

			el: "#form",

			template: _.template($("#form-template").html()),

			events: {
				'submit': 'filter',
				'reset': 'displayAll',
			},

			filter: function(e) {
				e.preventDefault();
				let ages = [];

				let age_model = new ageModel;

				let newMinAge = this.$el.find('input[id="ageMin"]');
				let newMaxAge = this.$el.find('input[id="ageMax"]');

				age_model.set('ageMin', newMinAge.val());
				age_model.set('ageMax', newMaxAge.val());

				console.log(age_model.getMinAge());
				console.log(age_model.getMaxAge());

				var currentTime = new Date();

				let gender = $('input[name=gender]:checked').val();

				var items = _.filter(this.model.attributes.results, function(json) {
					var items = currentTime.getFullYear() - new Date(json.dob).getFullYear();
					if(gender == "any") {
						if((items >= age_model.getMinAge()) && (items <= age_model.getMaxAge())) {
							return json;
						}
					} else {
						if((items >= age_model.getMinAge()) && (items <= age_model.getMaxAge() && json.gender == gender)) {
							return json;
						}
					}
				});

				this.$el.html(this.template({'results':items}));
				_.each({'results':items}, this.processItem, this);
			},

			displayAll: function() {
				this.$el.html(this.template(this.model.toJSON()));

				let current = new currentView({
					model: this.model	
				});

				current.render();
			},

			displayMen: function() {
				let items = this.collection.filterByGender(this.model.attributes.results, "male");
				console.log(items);
				this.$el.html(this.template({'results':items}));
				_.each({'results':items}, this.processItem, this);
			},

			displayWomen: function() {
				let items = this.collection.filterByGender(this.model.attributes.results, "female");
				this.$el.html(this.template({'results':items}));
				_.each({'results':items}, this.processItem, this);
			},

			processItem: function(item) {
				let current = new currentView({
					model: item
				});
				current.re_render();
			},

			render: function() {
				this.$el.html(this.template(this.model.toJSON()));
				return this;
			}
		});
		return formView;
	});
